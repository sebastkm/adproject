from fenics import *

mesh = IntervalMesh(10, 0, 2)
print type(5*dx(domain=mesh))
 
# A class to represent the "tape" data structure
class Tape(object):
 
    # This list isn't the tape itself, it's a list of "registered"
    # adjoint jacobian function implementations. It is populated once,
    # during module initialisation.
    adjoint_jacobians = []    
 
    def __init__(self):
        # The tape storage itself
        self.tape = []
        # A list of locations of variables within the tape, so that
        # they can be reset to zero when needed
        self.variable_positions = []
        # The list of ultimate adjoint Jacobian outputs, shown in the
        # diagram as being at the start of the tape, are here held in
        # a separate list. Negative variable indices are used to
        # reference these outputs and distinguish them from the
        # variables in the tape.
        self.outputs = []
        # The current position in the tape - only has meaning during
        # evaluation. Of course, holding this information (not to
        # mention the variables themselves) within the tape is pretty
        # dubious, from a state management / thread-safety point of
        # view.
        self.eval_position = 0
 
    # A class method for registering an adjoint jacobian implementation
    # function. Returns a value that can be used as a tag in the tape.
    @classmethod
    def register_adjoint_jacobian(cls, adjoint_jacobian):
        cls.adjoint_jacobians.append(adjoint_jacobian)
        return len(cls.adjoint_jacobians) - 1
 
    # Grow the tape by appending a fixed value (will be a "black"
    # input, a function tag or a variable reference)
    def append_constant(self, contents):
        self.tape.append(contents)
        self.eval_position += 1
 
    # Grow the tape by allocating a variable slot at the end
    def append_variable(self):
        pos = len(self.tape)
        self.tape.append(0)
        self.variable_positions.append(pos)
        return pos
 
    # Add an overall adjoint Jacobian output to the outputs list. This
    # happens when an "initial" ADReverseDouble is created - i.e. one
    # that doesn't arise from a previous AD-enabled operation.
    def add_output(self):
        self.outputs.append(0)
        return -len(self.outputs)
 
    # Add a quantity to the variable referenced by the value in the
    # current slot, which may be a variable in the tape (+ve ref) or
    # an entry in self.outputs (-ve ref)
    def add_to_referenced_variable(self, value):
        ref = self.tape[self.eval_position]
        if ref >=0:
            self.tape[ref] += value
        else:
            self.outputs[-ref-1] += value
 
    # Directly write to a variable at a particular position. This is
    # needed when setting an overall adjoint Jaobian input, via an
    # ADReverseDouble for a tape evaluation.
    def write_to_variable_at_index(self, idx, contents):
        if idx >=0:
            self.tape[idx] = contents
        else:
            self.outputs[-idx-1] = contents
 
    # Directly read a variable at a particular index. This is needed
    # when reading the adjoint Jacobian output from an
    # ADReverseDouble.
    def read_variable_at_index(self, idx):
        if idx >=0:
            return self.tape[idx]
        else:
            return self.outputs[-idx-1]
 
    # Read the value at the current location in the tape (during
    # evaluation)
    def read_current_value(self):
        return self.tape[self.eval_position]
 
    # Step back one position in the tape (during evaluation)
    def step_back(self):
        self.eval_position -= 1
 
    # Reset all variables and outputs to zero. Needed the tape has to
    # be evaluated for multiple adjoint Jacobian inputs.
    def reset_variables(self):
        for pos in self.variable_positions:
            self.tape[pos] = 0
        for idx in range(0,len(self.outputs)):
            self.outputs[idx] = 0
 
    # Evaluate the tape.
    def evaluate(self):
        self.eval_position = len(self.tape)
        while self.eval_position != 0:  
            self.step_back()
            # Look up the adjoint_jacobian according to the tag at the
            # current position in the tape, and call it
            self.adjoint_jacobians[self.tape[self.eval_position]](tape)


class ADConstant(object):
	# Class method to create an initial float, i.e. one that doesn't
    # result from a previous AD-enabled operation. adj_jac_output_ref
    # will be negative, referring to an entry in the ouputs list in
    # the tape object.
    @classmethod
    def create_initial(cls, value, tape):
        ret = ADConstant()
        ret.value = value
        ret.tape = tape
        ret.adj_jac_output_ref = tape.add_output()
        return ret

    # Class method to create a float that result from a previous
    # AD-enabled operation. adj_jac_output_ref will be >= 0, referring
    # to a location in the tape, which will be an adjoint Jacobian
    # input for the operation that produced this value.
    @classmethod
    def create_result(cls, value, tape, adj_jac_output_ref):
        ret = ADConstant()
        ret.value = value
        ret.tape = tape
        ret.adj_jac_output_ref = adj_jac_output_ref
        return ret

    # Used to set an input to the overall adjoint Jacobian. This
    # should only be called on ADReverseFloats that represent ultimate
    # outputs of the calculation.
    def set_initial_adj_jac_input(self, value):
        self.tape.write_to_variable_at_index(self.adj_jac_output_ref, value)

    def get_adj_jac_output(self):
        return self.tape.read_variable_at_index(self.adj_jac_output_ref)

    def __mul__(self, other):
        if type(other).__name__ == "Measure":
            self.tape.append_constant(self.adj_jac_output_ref)
            self.tape.append_constant(other)

            ref = self.tape.append_variable()

            self.tape.append_constant(self.value)
            self.tape.append_constant(other)

            self.tape.append_constant(self.MUL_TAG)

            return ADConstant.create_result(self.value * other, self.tape, ref)

        # Write the references to where to store the adjoint Jacobian
        # outputs
        self.tape.append_constant(self.adj_jac_output_ref)
        self.tape.append_constant(other.adj_jac_output_ref)
        # Allocate a variable to store the ajoint Jacobian input
        ref = self.tape.append_variable()
        # Write the values of self and other, as they will be needed
        self.tape.append_constant(self.value)
        self.tape.append_constant(other.value)
        # Store the function tag
        self.tape.append_constant(self.MUL_TAG)
        # The returned ADReverseFloat references the variable allocated above
        return ADConstant.create_result(self.value * other.value, self.tape, ref)

        #if type(other).__name__ == "Measure":
        #	return ADConstant(self.val * other, self.delta*other)
        #return ADConstant(self.val * other.val, other.val * self.delta + self.val * other.delta)

    # Implementation of the adjoint Jacobian for the * operatorq.  The
    # formula can be checked by working out the adjoint Jacobian
    # matrix by hand.
    def adj_jac__mul__(tape):
        # Fetch the original function inputs
        tape.step_back()        
        other_value = tape.read_current_value()
        tape.step_back()
        self_value = tape.read_current_value()
        # Fetch the adjoint Jacobian input
        tape.step_back()
        adj_jac_input = tape.read_current_value()
        # Add the adjoint Jacobian outputs to the appropriate
        # variables
        tape.step_back()
        tape.add_to_referenced_variable(adj_jac_input * self_value)
        tape.step_back()
        tape.add_to_referenced_variable(adj_jac_input * other_value)
 
    # Statically register the adjoint Jacobian function for the add
    # operator with the Tape class
    MUL_TAG = Tape.register_adjoint_jacobian(adj_jac__mul__)

#	def __pow__(self, other):
#		return ADConstant(self.val ** other)


def ad_assemble(form):
    return ADConstant(assemble(form.val), assemble(form.delta))

mesh = IntervalMesh(10, 0, 2)
V = FunctionSpace(mesh, "Lagrange", 1)

tape = Tape()

f = ADConstant.create_initial(3, tape)
#print f.vector().array()

#f.set_initial_adj_jac_input(1.0)

def fwd(f):
    form = f*f*dx(domain=mesh)   # integrate f over the domain
    return form #ad_assemble(form)

i = fwd(f)

i.set_initial_adj_jac_input(1.0)

tape.evaluate()

print f.get_adj_jac_output()

#print "Integral f over unitsquare: ", i.val
#print "Derivative: ", i.delta

# Goal: Overload assemble and compute derivative of fwd with respect to f.
#print f.get_adj_jac_output()


#
#
# @param ufl.form.Form form
def ad_assemble(form):
    pass

