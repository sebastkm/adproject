from fenics import *

class ADFunction(Function):
    def get_adj_jac_output(self):
        return 1

def ad_assemble(f):
    # ...
    pass

mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, "Lagrange", 1)

f = ADFunction(V)
f.vector()[:] = 1.5
print f.vector().array()

def fwd(f):
    form = f**2*dx   # integrate f over the domain
    return assemble(form)

i = fwd(f)
print "Integral f over unitsquare: ", i

# Goal: Overload assemble and compute derivative of fwd with respect to f.
print f.get_adj_jac_output()
