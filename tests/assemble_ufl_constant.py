from fenics import *

saved_form = None

class ADConstant():
    def __init__(self, val):
        self.val = val
        self.var = variable(val)

    def __mul__(self, other):
        if type(other).__name__ == "Measure":
            return self.var*other
        return ADConstant(self.var*other.var)

    def get_derivative(self):
        nform = diff(saved_form, self.var)
        return assemble(nform)

def ad_assemble(form):
    saved_form = form
    return assemble(form)

mesh = IntervalMesh(10, 0, 2)
V = FunctionSpace(mesh, "Lagrange", 1)

f = 3

def fwd(f):
    f = variable(f)
    form = f*f*dx(domain=mesh)   # integrate f over the domain
    return [assemble(form), assemble(diff(form, f))]

i = fwd(f)
print "Integral f over unitsquare: ", i[0]
print "Derivative: ", i[1]

# Goal: Overload assemble and compute derivative of fwd with respect to f.
#print f.get_adj_jac_output()


