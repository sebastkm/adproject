from fenics import *


class ADConstant():
	def __init__(self, val, delta):
		self.val = val
		self.delta = delta

	def __mul__(self, other):
		if type(other).__name__ == "Measure":
			return ADConstant(self.val * other, self.delta*other)
		return ADConstant(self.val * other.val, other.val * self.delta + self.val * other.delta)

#	def __pow__(self, other):
#		return ADConstant(self.val ** other)


def ad_assemble(form):
    return ADConstant(assemble(form.val), assemble(form.delta))

mesh = IntervalMesh(10, 0, 2)
V = FunctionSpace(mesh, "Lagrange", 1)

f = ADConstant(3, 1.0)
#print f.vector().array()

def fwd(f):
    form = f*f*dx(domain=mesh)   # integrate f over the domain
    return ad_assemble(form)

i = fwd(f)
print "Integral f over unitsquare: ", i.val
print "Derivative: ", i.delta

# Goal: Overload assemble and compute derivative of fwd with respect to f.
#print f.get_adj_jac_output()


