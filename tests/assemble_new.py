from fenics import *


class ADConstant():
	


def ad_assemble(form):
    

mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, "Lagrange", 1)

f = ADConstant(2)
print f.vector().array()

def fwd(f):
    form = f**2*dx   # integrate f over the domain
    return ad_assemble(form)

i = fwd(f)
print "Integral f over unitsquare: ", i

# Goal: Overload assemble and compute derivative of fwd with respect to f.
print f.get_adj_jac_output()
